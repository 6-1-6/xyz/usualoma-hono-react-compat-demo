Manual fix needed:
node_modules/@tamagui/web/dist/esm/views/Slot.mjs:
> remove importing 'version' from react
> fix the reference to version to say '18', like: const is19 = "18".startsWith("19."),

Needs also fixing the below:
Missing './jsx/dom/client' specifier in 'hono' package [plugin vite:dep-pre-bundle]
import { createRoot as domCreateRoot, hydrateRoot as domHydrateRoot } from 'react-dom/client';
