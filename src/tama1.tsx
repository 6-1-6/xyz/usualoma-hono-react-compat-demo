'use client'

// import { useState, useRef, useEffect } from 'hono/jsx';

import tamaguiConfig from '../tamagui.config.ts'

// import { Spinner } from 'tamagui'
// * causes:
//      Missing './jsx/dom/client' specifier in 'hono' package [plugin vite:dep-pre-bundle]
//      import { createRoot as domCreateRoot, hydrateRoot as domHydrateRoot } from 'react-dom/client';
import { TamaguiProvider, View, Text } from '@tamagui/core'
// import { Toast, useToastController, useToastState } from '@tamagui/toast'
import { XStack, YStack } from '@tamagui/stacks'
import { SizableText, Paragraph } from '@tamagui/text'
import { Button } from '@tamagui/button'
import { Input } from '@tamagui/input'
import { Label } from '@tamagui/label'
// import { Switch } from '@tamagui/switch'
import { Flame, Activity } from '@tamagui/lucide-icons'

export function TamaDemo1() {

  return (
    <TamaguiProvider
      config={tamaguiConfig}
    >
      <View width={'30em'} backgroundColor='#dcf' space='$4'>
        <YStack space='$2' alignItems='center' backgroundColor='#ddf'>
          <SizableText size='$3'>SizableText</SizableText>
          <XStack space>
            <SizableText theme='alt1' size='$3'>
              alt1
            </SizableText>
            <SizableText theme='alt2' size='$3'>
              alt2
            </SizableText>
          </XStack>
          <Paragraph size='$2' fontWeight='800'>
            Paragraph
          </Paragraph>
        </YStack>
        <YStack padding='$3' minWidth={300} space='$4'>
          <XStack alignItems='center' space='$4'>
            <Label width={90} htmlFor='name' marginLeft='$2'>
              Name
            </Label>
            <Input flex={1} id='name' defaultValue='Nate Wienert' />
          </XStack>
        </YStack>
        <YStack space='$2' alignItems='center' margin='$2'>
          <Text> In RN we have a Text element </Text>
          <Button icon={Flame} iconAfter={Activity} size='$4' space='$2'>Toast me when the ./jsx/dom/client problem solved :) </Button>
        </YStack>
      </View>
    </TamaguiProvider>
  )
}

// <XStack alignItems='center' space='$4'>
//   <Label width={90} htmlFor='notify'>
//     Notifications
//   </Label>
//   <Switch id='notify'>
//     <Switch.Thumb animation='quick' />
//   </Switch>
// </XStack>


// <YStack padding='$3' space='$4' alignItems='center'>
//   {<Spinner size='small' color='$green10' />
//   <Spinner size='large' color='$orange10' />}
// </YStack>


// export function TamaDemo2Toast () {
//   const [open, setOpen] = useState(false)
//   const timerRef = useRef(0)

//   useEffect(() => {
//     return () => clearTimeout(timerRef.current)
//   }, [])

//   return (
//     <YStack ai='center'>
//       <Button
//         onPress={() => {
//           setOpen(false)
//           window.clearTimeout(timerRef.current)
//           timerRef.current = window.setTimeout(() => {
//             setOpen(true)
//           }, 150)
//         }}
//       >
//         Single Toast
//       </Button>
//       <Toast
//         onOpenChange={setOpen}
//         open={open}
//         animation='100ms'
//         enterStyle={{ x: -20, opacity: 0 }}
//         exitStyle={{ x: -20, opacity: 0 }}
//         opacity={1}
//         x={0}
//       >
//         <Toast.Title>Subscribed!</Toast.Title>
//         <Toast.Description>We'll be in touch.</Toast.Description>
//       </Toast>
//     </YStack>
//   )
// }
