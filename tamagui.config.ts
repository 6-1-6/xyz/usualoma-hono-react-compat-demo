import { config } from '@tamagui/config/v3'
import { } from 'tamagui'
import { createTamagui as createTamaguiCore } from '@tamagui/core'

// const tamaguiConfig = createTamagui(config)
const tamaguiConfigCore = createTamaguiCore(config)

export type TamaguiConfigCore = typeof tamaguiConfigCore
export type TamaguiConfig = TamaguiConfigCore

declare module '@tamagui/core' {
  interface TamaguiCustomConfig extends TamaguiConfigCore {}
}
declare module 'tamagui' {
  // or '@tamagui/core'
  // overrides TamaguiCustomConfig so your custom types
  // work everywhere you import `tamagui`
  interface TamaguiCustomConfig extends TamaguiConfig {}
}
export default tamaguiConfigCore
