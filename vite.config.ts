import path from "path";
import { defineConfig } from "vite";
import { tamaguiExtractPlugin, tamaguiPlugin } from "@tamagui/vite-plugin";

export default defineConfig({
  resolve: {
    alias: {
      react: "hono/jsx/dom",
      "react-dom": "hono/jsx/dom",
      '@': path.resolve(__dirname)
    },
  },
  plugins: [
    tamaguiPlugin() as any,
    tamaguiExtractPlugin({
      logTimings: true,
    }),
  ],
});
